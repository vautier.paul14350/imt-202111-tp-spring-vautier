INSERT INTO course(name,description) VALUES('Maths','Des maths');
INSERT INTO course(name,description) VALUES('Anglais','De l\''anglais');
INSERT INTO course(name,description) VALUES('Francais','Français tavu');
INSERT INTO course(name,description) VALUES('Info','P''tit java oklm');
INSERT INTO course(name,description) VALUES('SSG','SSG très sérieux et corporate');
INSERT INTO course(name,description) VALUES('Coq','Pitié je comprends R');

INSERT INTO student(name,family_name) VALUES('Jean','Pierre');
INSERT INTO student(name,family_name) VALUES('Pierre','Pierre');
INSERT INTO student(name,family_name) VALUES('Emrik','Pierre');
INSERT INTO student(name,family_name) VALUES('Pol','Pierre');
INSERT INTO student(name,family_name) VALUES('Brugo','Pierre');

INSERT INTO course_follow(course_id,student_id,from_date,to_date) VALUES(1,1,to_date('2021-02-08','YYYY-MM-DD'),to_date('2021-05-08','YYYY-MM-DD'));
INSERT INTO course_follow(course_id,student_id,from_date,to_date) VALUES(3,1,to_date('2021-02-10','YYYY-MM-DD'),to_date('2021-07-08','YYYY-MM-DD'));
INSERT INTO course_follow(course_id,student_id,from_date,to_date) VALUES(2,2,to_date('2021-02-09','YYYY-MM-DD'),to_date('2021-06-08','YYYY-MM-DD'));
INSERT INTO course_follow(course_id,student_id,from_date,to_date) VALUES(4,3,to_date('2021-02-11','YYYY-MM-DD'),to_date('2021-08-08','YYYY-MM-DD'));

INSERT INTO grade(grade,follow_id,date) VALUES(8,1,to_date('2021-02-11','YYYY-MM-DD'));
INSERT INTO grade(grade,follow_id,date) VALUES(9,1,to_date('2021-05-11','YYYY-MM-DD'));
INSERT INTO grade(grade,follow_id,date) VALUES(10,2,to_date('2021-04-11','YYYY-MM-DD'));
INSERT INTO grade(grade,follow_id,date) VALUES(11,2,to_date('2021-06-11','YYYY-MM-DD'));
INSERT INTO grade(grade,follow_id,date) VALUES(12,3,to_date('2021-08-11','YYYY-MM-DD'));