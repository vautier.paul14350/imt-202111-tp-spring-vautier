create table student
(
    id          bigint generated always as identity
        constraint student_pkey
            primary key,
    name        varchar,
    family_name varchar
);

alter table student
    owner to postgres;

create table course
(
    id          bigint generated always as identity
        constraint course_pk
            primary key,
    name        varchar,
    description varchar
);

alter table course
    owner to postgres;

create table course_follow
(
    course_id  bigint
        constraint course_id__fk
            references course
            on delete cascade,
    student_id bigint
        constraint student_id__fk
            references student
            on delete cascade,
    from_date  date,
    to_date    date,
    id         bigint generated always as identity (maxvalue 2147483647)
        constraint course_follow_pk
            primary key
);

alter table course_follow
    owner to postgres;

create table grade
(
    grade     double precision not null
        constraint grade_in_bounds
            check ((grade >= (0)::double precision) AND (grade <= (20)::double precision)),
    follow_id bigint           not null
        constraint grade_course_follow_id_fk
            references course_follow
            on delete cascade,
    date      date             not null,
    constraint grade_pk
        primary key (date, follow_id)
);

alter table grade
    owner to postgres;