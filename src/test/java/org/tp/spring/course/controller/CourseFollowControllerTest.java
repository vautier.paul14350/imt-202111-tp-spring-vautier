package org.tp.spring.course.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tp.spring.course.entity.Course;
import org.tp.spring.course.entity.CourseFollow;
import org.tp.spring.course.entity.Student;
import org.tp.spring.course.exceptions.CourseFollowNotFoundException;
import org.tp.spring.course.service.impl.CourseFollowService;

import java.time.LocalDate;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CourseFollowControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CourseFollowService service;


    @Test
    public void testCreateCourseFollow() throws Exception {
        CourseFollow cf = new CourseFollow();
        cf.setId(1L);
        when(service.createCourseFollow(any())).thenReturn(cf);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/follow")
                .content("{\"student_id\":2,\"course_id\":2}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$", is(1)));
    }

    @Test
    public void testGetCourseFollow() throws Exception {
        Student student = new Student(12L, "Name", "FamilyName", Collections.emptySet());
        Course course = new Course(12L, "Maths", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.of(2021, 12, 01);
        CourseFollow returned = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        when(service.getCourseFollowById(any(Long.class))).thenReturn(returned);

        mockMvc.perform(get("/api/follow/{id}", 2))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(12)));
    }

    @Test
    public void testGetCourseFollowException() throws Exception {
        CourseFollowNotFoundException e = new CourseFollowNotFoundException(1L);
        doThrow(e).when(service).getCourseFollowById(any(Long.class));
        mockMvc.perform(get("/api/follow/{id}", 2))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status", is("error")))
                .andExpect(jsonPath("$.message", is(e.getMessage())));
    }

    @Test
    public void testDeleteCourseFollow() throws Exception {
        doNothing().when(service).deleteCourseFollow(any(Long.class));
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/follow/{id}", 12L))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testDeleteCourseFollowException() throws Exception {
        CourseFollowNotFoundException e = new CourseFollowNotFoundException(1L);
        doThrow(e).when(service).deleteCourseFollow(any(Long.class));
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/follow/{id}", 12L))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status", is("error")))
                .andExpect(jsonPath("$.message", is(e.getMessage())));
    }

}
