package org.tp.spring.course.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.tp.spring.course.entity.Course;
import org.tp.spring.course.exceptions.CourseNotFoundException;
import org.tp.spring.course.service.impl.CourseService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CourseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CourseService service;

    @Test
    public void testGetAllCourses() throws Exception {
        Course course1 = new Course(12L, "Maths", "Description", Collections.emptySet());
        Course course2 = new Course(13L, "Français", "Description", Collections.emptySet());
        List<Course> list = new ArrayList<>();
        list.add(course1);
        list.add(course2);
        when(service.getAllCourses()).thenReturn(list);
        mockMvc.perform(get("/api/courses"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is("Maths")))
                .andExpect(jsonPath("$[0].description", is("Description")));
    }

    @Test
    public void testGetCourses() throws Exception {
        Course returned = new Course(12L, "Maths", "Description", Collections.emptySet());
        when(service.getCourseById(any(Long.class))).thenReturn(returned);

        mockMvc.perform(get("/api/courses/{id}", 2))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Maths")))
                .andExpect(jsonPath("$.description", is("Description")));
    }

    @Test
    public void testGetCoursesException() throws Exception {
        CourseNotFoundException e = new CourseNotFoundException(2L);
        when(service.getCourseById(any(Long.class))).thenThrow(e);

        mockMvc.perform(get("/api/courses/{id}", 2))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status", is("error")))
                .andExpect(jsonPath("$.message", is(e.getMessage())));
    }

    @Test
    public void testCreateCourse() throws Exception {
        Course returned = new Course(12L, "MatièreDeTest", "DescriptionDeTest", Collections.emptySet());
        when(service.createCourse(any(Course.class))).thenReturn(returned);
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/courses")
                        .content("{\"name\":\"MatièreDeTest\",\"description\":\"DescriptionDeTest\"}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNumber())
                .andExpect(jsonPath("$", is(12)));
    }

    @Test
    public void testDeleteCourse() throws Exception {
        doNothing().when(service).deleteCourse(any(Long.class));
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/courses/{id}", 12L))
                .andExpect(status().isAccepted());
    }

    @Test
    public void testDeleteCourseException() throws Exception {
        CourseNotFoundException e = new CourseNotFoundException(2L);
        doThrow(e).when(service).deleteCourse(any(Long.class));

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/courses/{id}", 2))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status", is("error")))
                .andExpect(jsonPath("$.message", is(e.getMessage())));
    }

}
