package org.tp.spring.course.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tp.spring.course.entity.Student;
import org.tp.spring.course.service.impl.StudentService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StudentService service;

    @Test
    public void testGetStudentById() throws Exception {
        Student returned = new Student(12L, "Name", "FamilyName", Collections.emptySet());
        when(service.getStudentById(any(Long.class))).thenReturn(returned);

        mockMvc.perform(get("/api/students/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Name")))
                .andExpect(jsonPath("$.family_name", is("FamilyName")));
    }

    @Test
    public void testGetAllStudents() throws Exception {
        Student student1 = new Student(12L, "Name", "FamilyName", Collections.emptySet());
        Student student2 = new Student(12L, "Name", "FamilyName", Collections.emptySet());
        List<Student> list = new ArrayList<>();
        list.add(student1);
        list.add(student2);

        when(service.getAllStudents()).thenReturn(list);

        mockMvc.perform(get("/api/students"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is("Name")))
                .andExpect(jsonPath("$[0].family_name", is("FamilyName")));
    }

    @Test
    public void testGetStudentByNameAndFamilyName() throws Exception {
        Student student1 = new Student(12L, "Name", "FamilyName", Collections.emptySet());
        Student student2 = new Student(13L, "Name", "FamilyName", Collections.emptySet());
        List<Student> list = new ArrayList<>();
        list.add(student1);
        list.add(student2);
        when(service.getStudentByNameAndFamilyName(any(String.class), any(String.class))).thenReturn(list);
        mockMvc.perform(get("/api/students/query")
                        .param("name", "Name")
                        .param("family_name", "FamilyName")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is("Name")))
                .andExpect(jsonPath("$[0].family_name", is("FamilyName")));
    }

    @Test
    public void testCreateStudent() throws Exception {
        Student returned = new Student(22L, "Aymeric", "Lecolazet", Collections.emptySet());
        when(service.createStudent(any(Student.class))).thenReturn(returned);

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/students")
                        .content("{\n" +
                                "    \"name\":\"Aymeric\",\n" +
                                "    \"family_name\":\"Lecolazet\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testDeleteStudent() throws Exception {
        doNothing().when(service).deleteStudent(any(Long.class));
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/students/{id}", 2))
                .andExpect(status().isAccepted());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
