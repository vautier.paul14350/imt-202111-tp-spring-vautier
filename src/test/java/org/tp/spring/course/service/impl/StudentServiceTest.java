package org.tp.spring.course.service.impl;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.tp.spring.course.entity.Student;
import org.tp.spring.course.exceptions.StudentNotFoundException;
import org.tp.spring.course.repository.StudentRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceTest {

    @Mock
    StudentRepository studentRepository;

    @InjectMocks
    StudentService studentService;

    @Test
    public void testCreateStudent() {

        Student returned = new Student(1L, "Aymeric", "Lecolazet", null);
        when(studentRepository.save(any(Student.class))).thenReturn(returned);

        Student created = studentService.createStudent(new Student());
        assertEquals(created.getName(), "Aymeric");
        assertEquals(created.getFamilyName(), "Lecolazet");
        assertEquals(created.getId().longValue(), 1L);
        assertEquals(created.getFollowedCourses(), null);
    }

    @Test
    public void testDeleteCourse() throws StudentNotFoundException {
        doNothing().when(studentRepository).deleteById(1L);
        when(studentRepository.existsById(1L)).thenReturn(true);
        studentService.deleteStudent(1L);

    }

    @Test
    public void testGetAllCourses() throws Exception {
        Student student1 = new Student(12L, "Maths", "Description", Collections.emptySet());
        Student student2 = new Student(13L, "Français", "Description", Collections.emptySet());
        List<Student> list = new ArrayList<>();
        list.add(student1);
        list.add(student2);
        when(studentRepository.findAll()).thenReturn(list);

        List<Student> allstudent = studentService.getAllStudents();

        System.out.println(allstudent.get(0).getId());

        assertEquals(12L, allstudent.get(0).getId().longValue());

    }

}
