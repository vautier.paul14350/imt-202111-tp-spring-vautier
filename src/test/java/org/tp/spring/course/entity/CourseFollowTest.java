package org.tp.spring.course.entity;

import org.junit.Test;

import java.time.LocalDate;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CourseFollowTest {

    @Test
    public void getIdCourseFollowTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();

        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        assertEquals(12L, courseFollow.getId());
    }

    @Test
    public void getStudentCourseFollowTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();

        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        assertEquals(student, courseFollow.getStudent());
    }

    @Test
    public void getCourseCourseFollowTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();

        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        assertEquals(course, courseFollow.getCourse());
    }

    @Test
    public void getFromDateCourseFollowTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();

        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        assertEquals(localDate, courseFollow.getFromDate());
    }

    @Test
    public void getToDateCourseFollowTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();

        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        assertEquals(localDate, courseFollow.getToDate());
    }

    @Test
    public void getGradingsCourseFollowTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();

        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        assertEquals(Collections.emptySet(), courseFollow.getGradings());
    }

    @Test
    public void setIdCourseFollowTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();

        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        courseFollow.setId(20L);
        assertEquals(20L, courseFollow.getId());
    }

    @Test
    public void setStudentCourseFollowTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();

        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        Student newStudent = new Student(12L, "newPrenom", "newNom", Collections.emptySet());

        courseFollow.setStudent(newStudent);
        assertEquals(newStudent, courseFollow.getStudent());
    }

    @Test
    public void setCourseCourseFollowTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();

        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        Course newCourse = new Course(12L, "NewPrenom", "NewDescription", Collections.emptySet());

        courseFollow.setCourse(newCourse);
        assertEquals(newCourse, courseFollow.getCourse());
    }

    @Test
    public void setFromDateCourseFollowTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();

        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        LocalDate newLocalDate = LocalDate.of(2020, 12, 20);

        courseFollow.setFromDate(newLocalDate);
        assertEquals(newLocalDate, courseFollow.getFromDate());
    }

    @Test
    public void setToDateCourseFollowTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();

        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        LocalDate newLocalDate = LocalDate.of(2020, 12, 20);

        courseFollow.setToDate(newLocalDate);
        assertEquals(newLocalDate, courseFollow.getToDate());
    }

    @Test
    public void toStringFollowTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.of(2021, 12, 01);

        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        assertEquals("CourseFollow(id=12, student=Student(id=12, name=Prenom, familyName=Nom), course=Course(id=12, name=Prenom, description=Description, followings=[]), fromDate=2021-12-01, toDate=2021-12-01, gradings=[])", courseFollow.toString());
    }

}
