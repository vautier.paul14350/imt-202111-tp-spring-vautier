package org.tp.spring.course.entity;

import org.junit.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StudentTest {

    @Test
    public void getIdStudentTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        student.getClass();
        assertEquals(12L, student.getId());
    }

    @Test
    public void setIdStudentTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        student.setId(20L);
        assertEquals(20L, student.getId());
    }

    @Test
    public void getNameStudentTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        assertEquals("Prenom", student.getName());
    }

    @Test
    public void setNameStudentTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        student.setName("Aymeric");
        assertEquals("Aymeric", student.getName());
    }

    @Test
    public void getFamilyNameStudentTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        assertEquals("Nom", student.getFamilyName());
    }

    @Test
    public void setFamilyNameStudentTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        student.setFamilyName("Lecolazet");
        assertEquals("Lecolazet", student.getFamilyName());
    }

    @Test
    public void getFollowedCoursesStudentTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        assertEquals(Collections.emptySet(), student.getFollowedCourses());
    }

    @Test
    public void setFollowedCoursesStudentTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        student.setFollowedCourses(Collections.emptySet());
        assertEquals(Collections.emptySet(), student.getFollowedCourses());
    }

    @Test
    public void toStringCourseTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        assertEquals("Student(id=12, name=Prenom, familyName=Nom)", student.toString());
    }

    @Test
    public void constructorIDTest() {
        Student newStudent = new Student();
        Student student = new Student(12L);
        assertEquals(12L, student.getId());
    }
}
