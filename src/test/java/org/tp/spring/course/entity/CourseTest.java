package org.tp.spring.course.entity;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class CourseTest {

    @Test
    public void getIdCourseTest() {
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        assertEquals(12L, course.getId());
    }

    @Test
    public void setIdCourseTest() {
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        course.setId(20L);
        assertEquals(20L, course.getId());
    }

    @Test
    public void getNameCourseTest() {
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        assertEquals("Prenom", course.getName());
    }

    @Test
    public void setNameCourseTest() {
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        course.setName("Aymeric");
        assertEquals("Aymeric", course.getName());
    }

    @Test
    public void getFamilyNameCourseTest() {
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        assertEquals("Description", course.getDescription());
    }

    @Test
    public void setFamilyNameCourseTest() {
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        course.setDescription("New Description");
        assertEquals("New Description", course.getDescription());
    }

    @Test
    public void getFollowingsCourseTest() {
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        assertEquals(Collections.emptySet(), course.getFollowings());
    }

    @Test
    public void setFollowingsCourseTest() {
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        course.setFollowings(Collections.emptySet());
        assertEquals(Collections.emptySet(), course.getFollowings());
    }

    @Test
    public void toStringCourseTest() {
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        assertEquals("Course(id=12, name=Prenom, description=Description, followings=[])", course.toString());
    }

    @Test
    public void constructorIdTest() {
        Course course = new Course(12L);
        assertEquals(12L, course.getId());
    }
}
