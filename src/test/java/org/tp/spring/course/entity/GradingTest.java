package org.tp.spring.course.entity;

import org.junit.Test;

import java.time.LocalDate;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GradingTest {

    @Test
    public void getGradeGradingTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();
        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());
        GradePK gradePK = new GradePK(localDate, courseFollow);

        Grading grade = new Grading(gradePK, 10D);

        assertEquals(10D, grade.getGrade());
    }

    @Test
    public void getGradePKGradingTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();
        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());
        GradePK gradePK = new GradePK(localDate, courseFollow);

        Grading grade = new Grading(gradePK, 10D);

        assertEquals(gradePK, grade.getGradeId());
    }

    @Test
    public void setGradePkGradingTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();
        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());
        GradePK gradePK = new GradePK(localDate, courseFollow);

        Grading grade = new Grading(gradePK, 10D);

        GradePK newGradePK = new GradePK(localDate, courseFollow);

        grade.setGradeId(gradePK);

        assertEquals(gradePK, grade.getGradeId());
    }

    @Test
    public void setGradeGradingTest() {
        Grading testgrade = new Grading();
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();
        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());
        GradePK gradePK = new GradePK(localDate, courseFollow);

        Grading grade = new Grading(gradePK, 10D);

        grade.setGrade(12D);

        assertEquals(12D, grade.getGrade());
    }

    @Test
    public void toStringFollowTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.of(2021, 12, 01);
        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());
        GradePK gradePK = new GradePK(localDate, courseFollow);

        Grading grade = new Grading(gradePK, 10D);

        assertEquals("Grading(gradeId=GradePK(date=2021-12-01, gradedCourse=CourseFollow(id=12, student=Student(id=12, name=Prenom, familyName=Nom), course=Course(id=12, name=Prenom, description=Description, followings=[]), fromDate=2021-12-01, toDate=2021-12-01, gradings=[])), grade=10.0)", grade.toString());
    }

}
