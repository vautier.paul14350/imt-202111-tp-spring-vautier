package org.tp.spring.course.entity;

import org.junit.Test;

import java.time.LocalDate;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GradePKTest {

    @Test
    public void getDateGradePKTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();
        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        GradePK gradePK = new GradePK(localDate, courseFollow);

        assertEquals(localDate, gradePK.getDate());
    }

    @Test
    public void getgradedCourseGradePKTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();
        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        GradePK gradePK = new GradePK(localDate, courseFollow);

        assertEquals(courseFollow, gradePK.getGradedCourse());
    }

    @Test
    public void setDateGradePKTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();
        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        GradePK gradePK = new GradePK(localDate, courseFollow);

        LocalDate newLocalDate = LocalDate.of(2020, 12, 02);
        gradePK.setDate(newLocalDate);

        assertEquals(newLocalDate, gradePK.getDate());
    }

    @Test
    public void setgradedCourseGradePKTest() {
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.now();
        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        GradePK gradePK = new GradePK(localDate, courseFollow);

        CourseFollow newCourseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());
        gradePK.setGradedCourse(newCourseFollow);

        assertEquals(newCourseFollow, gradePK.getGradedCourse());
    }

    @Test
    public void toStringGradePKTest() {
        GradePK testGradePK = new GradePK();
        Student student = new Student(12L, "Prenom", "Nom", Collections.emptySet());
        Course course = new Course(12L, "Prenom", "Description", Collections.emptySet());
        LocalDate localDate = LocalDate.of(2021, 12, 01);
        CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate, Collections.emptySet());

        GradePK gradePK = new GradePK(localDate, courseFollow);

        assertEquals("GradePK(date=2021-12-01, gradedCourse=CourseFollow(id=12, student=Student(id=12, name=Prenom, familyName=Nom), course=Course(id=12, name=Prenom, description=Description, followings=[]), fromDate=2021-12-01, toDate=2021-12-01, gradings=[]))", gradePK.toString());
    }

}
