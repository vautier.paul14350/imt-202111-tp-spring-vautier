package org.tp.spring.course.mapstruct.dtos;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StudentListGetDtoTest {

    @Test
    void getListStudentGetDto() {
        StudentListGetDto testStudentGetDto = new StudentListGetDto();
        StudentGetDto studentGetDto = new StudentGetDto(12L, "Prenom", "Nom", Collections.emptySet());
        List<StudentGetDto> listStudentGetDto = new ArrayList<>();
        listStudentGetDto.add(studentGetDto);

        StudentListGetDto studentListGetDto = new StudentListGetDto(listStudentGetDto);

        assertEquals(listStudentGetDto, studentListGetDto.getListStudentGetDto());
    }

    @Test
    void setListStudentGetDto() {
        StudentGetDto studentGetDto = new StudentGetDto(12L, "Prenom", "Nom", Collections.emptySet());
        List<StudentGetDto> listStudentGetDto = new ArrayList<>();
        listStudentGetDto.add(studentGetDto);
        StudentListGetDto studentListGetDto = new StudentListGetDto(listStudentGetDto);

        StudentGetDto anotherStudentGetDto = new StudentGetDto(13L, "anotherPrenom", "anotherNom", Collections.emptySet());
        List<StudentGetDto> anotherListStudentGetDto = new ArrayList<>();
        anotherListStudentGetDto.add(anotherStudentGetDto);

        studentListGetDto.setListStudentGetDto(anotherListStudentGetDto);

        assertEquals(anotherListStudentGetDto, studentListGetDto.getListStudentGetDto());
    }
}