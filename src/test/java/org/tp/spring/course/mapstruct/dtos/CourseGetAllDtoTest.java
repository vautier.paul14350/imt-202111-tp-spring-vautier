package org.tp.spring.course.mapstruct.dtos;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CourseGetAllDtoTest {

    @Test
    void getId() {
        CourseGetAllDto courseGetAllDto = new CourseGetAllDto(12L, "Prenom", "Description");
        assertEquals(12L, courseGetAllDto.getId());
    }

    @Test
    void getName() {
        CourseGetAllDto courseGetAllDto = new CourseGetAllDto(12L, "Prenom", "Description");
        assertEquals("Prenom", courseGetAllDto.getName());
    }

    @Test
    void getDescription() {
        CourseGetAllDto courseGetAllDto = new CourseGetAllDto(12L, "Prenom", "Description");
        assertEquals("Description", courseGetAllDto.getDescription());
    }

    @Test
    void setId() {
        CourseGetAllDto courseGetAllDto = new CourseGetAllDto(12L, "Prenom", "Description");
        courseGetAllDto.setId(13L);
        assertEquals(13L, courseGetAllDto.getId());
    }

    @Test
    void setName() {
        CourseGetAllDto courseGetAllDto = new CourseGetAllDto(12L, "Prenom", "Description");
        courseGetAllDto.setName("NewPrenom");
        assertEquals("NewPrenom", courseGetAllDto.getName());
    }

    @Test
    void setDescription() {
        CourseGetAllDto courseGetAllDto = new CourseGetAllDto(12L, "Prenom", "Description");
        courseGetAllDto.setDescription("NewDescription");
        assertEquals("NewDescription", courseGetAllDto.getDescription());
    }
}