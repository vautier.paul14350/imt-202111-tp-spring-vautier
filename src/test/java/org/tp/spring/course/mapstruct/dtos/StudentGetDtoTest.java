package org.tp.spring.course.mapstruct.dtos;

import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StudentGetDtoTest {

    @Test
    void getId() {
        StudentGetDto studentGetDto = new StudentGetDto(12L, "Prenom", "Nom", Collections.emptySet());
        assertEquals(12L, studentGetDto.getId());
    }

    @Test
    void getName() {
        StudentGetDto studentGetDto = new StudentGetDto(12L, "Prenom", "Nom", Collections.emptySet());
        assertEquals("Prenom", studentGetDto.getName());
    }

    @Test
    void getFamilyName() {
        StudentGetDto studentGetDto = new StudentGetDto(12L, "Prenom", "Nom", Collections.emptySet());
        assertEquals("Nom", studentGetDto.getFamilyName());
    }

    @Test
    void getFollowedCourses() {
        StudentGetDto studentGetDto = new StudentGetDto(12L, "Prenom", "Nom", Collections.emptySet());
        assertEquals(Collections.emptySet(), studentGetDto.getFollowedCourses());
    }

    @Test
    void setId() {
        StudentGetDto studentGetDto = new StudentGetDto(12L, "Prenom", "Nom", Collections.emptySet());
        studentGetDto.setId(14L);
        assertEquals(14L, studentGetDto.getId());
    }

    @Test
    void setName() {
        StudentGetDto studentGetDto = new StudentGetDto(12L, "Prenom", "Nom", Collections.emptySet());
        studentGetDto.setName("NewPrenom");
        assertEquals("NewPrenom", studentGetDto.getName());
    }

    @Test
    void setFamilyName() {
        StudentGetDto studentGetDto = new StudentGetDto(12L, "Prenom", "Nom", Collections.emptySet());
        studentGetDto.setFamilyName("NewNom");
        assertEquals("NewNom", studentGetDto.getFamilyName());
    }

    @Test
    void setFollowedCourses() {
        StudentGetDto studentGetDto = new StudentGetDto(12L, "Prenom", "Nom", Collections.emptySet());
        studentGetDto.setFollowedCourses(Collections.emptySet());
        assertEquals(Collections.emptySet(), studentGetDto.getFollowedCourses());
    }

    @Test
    void noArgsTest() {
        StudentGetDto studentGetDto = new StudentGetDto();
    }
}