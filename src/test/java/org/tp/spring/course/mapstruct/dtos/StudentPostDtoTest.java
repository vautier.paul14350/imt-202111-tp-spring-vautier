package org.tp.spring.course.mapstruct.dtos;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class StudentPostDtoTest {

    @Test
    public void getIdStudentPostDtoTest() {
        StudentPostDto studentPostDto = new StudentPostDto(12L, "Prenom", "Nom");
        assertEquals(12L, studentPostDto.getId());
    }

    @Test
    public void setIdStudentPostDtoTest() {
        StudentPostDto studentPostDto = new StudentPostDto(12L, "Prenom", "Nom");
        studentPostDto.setId(20L);
        assertEquals(20L, studentPostDto.getId());
    }

    @Test
    public void getNameStudentPostDtoTest() {
        StudentPostDto studentPostDto = new StudentPostDto(12L, "Prenom", "Nom");
        assertEquals("Prenom", studentPostDto.getName());
    }

    @Test
    public void setNameStudentPostDtoTest() {
        StudentPostDto studentPostDto = new StudentPostDto(12L, "Prenom", "Nom");
        studentPostDto.setName("Aymeric");
        assertEquals("Aymeric", studentPostDto.getName());
    }

    @Test
    public void getFamilyNameStudentPostDtoTest() {
        StudentPostDto studentPostDto = new StudentPostDto(12L, "Prenom", "Nom");
        assertEquals("Nom", studentPostDto.getFamilyName());
    }

    @Test
    public void setFamilyNameStudentPostDtoTest() {
        StudentPostDto testStudentPostDto = new StudentPostDto();
        StudentPostDto studentPostDto = new StudentPostDto(12L, "Prenom", "Nom");
        studentPostDto.setFamilyName("Lecolazet");
        assertEquals("Lecolazet", studentPostDto.getFamilyName());
    }
}
