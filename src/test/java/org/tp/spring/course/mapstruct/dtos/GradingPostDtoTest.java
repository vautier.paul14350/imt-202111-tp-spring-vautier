package org.tp.spring.course.mapstruct.dtos;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GradingPostDtoTest {

    @Test
    void getGrade() {
        LocalDate localDate = LocalDate.now();
        GradingPostDto gradingPostDto = new GradingPostDto(20D, localDate, 20L);
        assertEquals(20D, gradingPostDto.getGrade());
    }

    @Test
    void getDate() {
        LocalDate localDate = LocalDate.now();
        GradingPostDto gradingPostDto = new GradingPostDto(20D, localDate, 20L);
        assertEquals(localDate, gradingPostDto.getDate());
    }

    @Test
    void getFollowId() {
        LocalDate localDate = LocalDate.now();
        GradingPostDto gradingPostDto = new GradingPostDto(20D, localDate, 20L);
        assertEquals(20L, gradingPostDto.getFollowId());
    }

    @Test
    void setGrade() {
        LocalDate localDate = LocalDate.now();
        GradingPostDto gradingPostDto = new GradingPostDto(20D, localDate, 20L);

        gradingPostDto.setGrade(18D);

        assertEquals(18D, gradingPostDto.getGrade());
    }

    @Test
    void setDate() {
        LocalDate localDate = LocalDate.now();
        GradingPostDto gradingPostDto = new GradingPostDto(20D, localDate, 20L);

        LocalDate newLocalDate = LocalDate.of(2020, 12, 02);
        gradingPostDto.setDate(newLocalDate);

        assertEquals(newLocalDate, gradingPostDto.getDate());
    }

    @Test
    void setFollowId() {
        GradingPostDto testGradingPostDto = new GradingPostDto();
        LocalDate localDate = LocalDate.now();
        GradingPostDto gradingPostDto = new GradingPostDto(20D, localDate, 20L);

        gradingPostDto.setFollowId(18L);

        assertEquals(18L, gradingPostDto.getFollowId());
    }
}