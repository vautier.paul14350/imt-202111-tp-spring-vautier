package org.tp.spring.course.mapstruct.dtos;

import org.junit.jupiter.api.Test;
import org.tp.spring.course.entity.Course;
import org.tp.spring.course.entity.CourseFollow;
import org.tp.spring.course.entity.Student;

import java.time.LocalDate;
import java.util.Collections;

class CourseFollowGetDtoTest {
    Student student = new Student();
    Course course = new Course();
    LocalDate localDate = LocalDate.of(2021, 12, 01);
    LocalDate localDate2 = LocalDate.of(2021, 12, 01);

    CourseFollow courseFollow = new CourseFollow(12L, student, course, localDate, localDate2, Collections.emptySet());


    @Test
    void studentSerialize() {
    }

    @Test
    void getId() {
    }

    @Test
    void getStudent() {
    }

    @Test
    void getCourse() {
    }

    @Test
    void getGradings() {
    }

    @Test
    void setId() {
    }

    @Test
    void setStudent() {
    }

    @Test
    void setCourse() {
    }

    @Test
    void setGradings() {
    }
}