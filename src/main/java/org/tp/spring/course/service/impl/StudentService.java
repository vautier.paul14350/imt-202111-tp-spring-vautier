package org.tp.spring.course.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.tp.spring.course.entity.Student;
import org.tp.spring.course.exceptions.StudentNotFoundException;
import org.tp.spring.course.repository.StudentRepository;
import org.tp.spring.course.service.contract.IStudentService;

import java.util.List;

@AllArgsConstructor
@Service
@Slf4j
public class StudentService implements IStudentService {
    private final StudentRepository studentRepository;

    @Override
    public Student getStudentById(Long id) throws StudentNotFoundException {
        return this.studentRepository.findById(id).orElseThrow(() -> new StudentNotFoundException(id));
    }

    @Override
    public List<Student> getStudentByNameAndFamilyName(String name, String familyName) {
        List<Student> list = this.studentRepository.findByFamilyNameAndName(familyName, name);
        log.info("Retrieved students by name {}' and familyName '{}' : {}", name, familyName, list);
        return list;
    }


    @Override
    public Student createStudent(Student student) {
        return this.studentRepository.save(student);
    }

    @Override
    public void deleteStudent(Long id) throws StudentNotFoundException {
        if (!exists(id))
            throw new StudentNotFoundException(id);
        this.studentRepository.deleteById(id);
    }


    @Override
    public List<Student> getAllStudents() {
        return (List<Student>) this.studentRepository.findAll();
    }

    @Override
    public boolean exists(Long id) {
        return studentRepository.existsById(id);
    }
}
