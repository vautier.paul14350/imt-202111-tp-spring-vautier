package org.tp.spring.course.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.tp.spring.course.entity.Course;
import org.tp.spring.course.exceptions.CourseNotFoundException;
import org.tp.spring.course.repository.CourseRepository;
import org.tp.spring.course.service.contract.ICourseService;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class CourseService implements ICourseService {
    private final CourseRepository courseRepository;

    @Override
    public Course getCourseById(Long id) throws CourseNotFoundException {
        return courseRepository.findById(id).orElseThrow(() -> new CourseNotFoundException(id));
    }

    @Override
    public Course createCourse(Course course) {
        return courseRepository.save(course);
    }

    @Override
    public void deleteCourse(Long id) throws CourseNotFoundException {
        if (!exists(id))
            throw new CourseNotFoundException(id);

        courseRepository.deleteById(id);
    }

    @Override
    public List<Course> getAllCourses() {
        List<Course> list = new ArrayList<>();
        courseRepository.findAll().forEach(list::add);
        return list;
    }

    @Override
    public boolean exists(Long id) {
        return courseRepository.existsById(id);
    }
}
