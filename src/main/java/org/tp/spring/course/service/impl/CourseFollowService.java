package org.tp.spring.course.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.tp.spring.course.entity.CourseFollow;
import org.tp.spring.course.entity.Grading;
import org.tp.spring.course.entity.Student;
import org.tp.spring.course.exceptions.CourseFollowNotFoundException;
import org.tp.spring.course.exceptions.CourseNotFoundException;
import org.tp.spring.course.exceptions.GradingNotFoundException;
import org.tp.spring.course.exceptions.StudentNotFoundException;
import org.tp.spring.course.repository.CourseFollowRepository;
import org.tp.spring.course.service.contract.GradeRepository;
import org.tp.spring.course.service.contract.ICourseFollowService;
import org.tp.spring.course.service.contract.ICourseService;
import org.tp.spring.course.service.contract.IStudentService;

import java.time.LocalDate;
import java.util.*;


@AllArgsConstructor
@Service
public class CourseFollowService implements ICourseFollowService {
    private final CourseFollowRepository courseFollowRepository;
    private final GradeRepository gradeRepository;

    private final ICourseService courseService;
    private final IStudentService studentService;

    @Override
    public CourseFollow createCourseFollow(CourseFollow courseFollow) throws CourseNotFoundException, StudentNotFoundException {
        Long studentId = courseFollow.getStudent().getId();
        Long courseId = courseFollow.getCourse().getId();

        if (!courseService.exists(courseId))
            throw new CourseNotFoundException(courseId);

        if (!studentService.exists(studentId))
            throw new StudentNotFoundException(studentId);

        return courseFollowRepository.save(courseFollow);
    }

    @Override
    public CourseFollow getCourseFollowById(Long id) throws CourseFollowNotFoundException {
        return courseFollowRepository.findById(id).orElseThrow(() -> new CourseFollowNotFoundException(id));
    }

    @Override
    public void deleteCourseFollow(Long id) throws CourseFollowNotFoundException {
        if (!courseFollowRepository.existsById(id))
            throw new CourseFollowNotFoundException(id);
        courseFollowRepository.deleteById(id);
    }

    @Override
    public Grading addGrade(Grading grading) throws CourseFollowNotFoundException {
        Long id = grading.getGradeId().getGradedCourse().getId();
        if (!courseFollowRepository.existsById(id))
            throw new CourseFollowNotFoundException(id);
        return gradeRepository.save(grading);
    }

    @Override
    public List<Student> getStudentsByMark(long courseId, float mark) {
        HashMap<Long, Student> studentMap = new HashMap<>();
        courseFollowRepository.findCoursesBelowGrade(courseId, mark).forEach(course -> {
            Long id = course.getStudent().getId();
            if (!studentMap.containsKey(id)) {
                studentMap.put(id, course.getStudent());
                course.getStudent().setFollowedCourses(new HashSet<CourseFollow>(Collections.singletonList(course)));
            } else {
                studentMap.get(id).getFollowedCourses().add(course);
            }
        });
        return new ArrayList<>(studentMap.values());
    }

    @Override
    public void deleteGradeByFollowIdAndDate(long id, LocalDate date) throws GradingNotFoundException {
        if (!gradeRepository.existsByGradeId_GradedCourse_IdAndGradeId_Date(id, date))
            throw new GradingNotFoundException(date, id);
        gradeRepository.deleteByGradeId_GradedCourse_IdAndGradeId_Date(id, date);
    }

    @Override
    public boolean exists(long id) {
        return courseFollowRepository.existsById(id);
    }
}
