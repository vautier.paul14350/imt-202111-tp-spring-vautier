package org.tp.spring.course.service.contract;

import org.tp.spring.course.entity.Course;
import org.tp.spring.course.exceptions.CourseNotFoundException;

import java.util.List;

public interface ICourseService {
    public Course getCourseById(Long id) throws CourseNotFoundException;

    public Course createCourse(Course student);

    public void deleteCourse(Long id) throws CourseNotFoundException;

    public List<Course> getAllCourses();

    public boolean exists(Long id);
}
