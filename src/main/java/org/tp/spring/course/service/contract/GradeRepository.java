package org.tp.spring.course.service.contract;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.tp.spring.course.entity.Grading;

import java.time.LocalDate;
import java.util.Optional;

@Repository
@Transactional
public interface GradeRepository extends CrudRepository<Grading, Long> {
    Optional<Grading> findByGradeId_GradedCourse_IdAndGradeId_Date(Long id, LocalDate date);

    void deleteByGradeId_GradedCourse_IdAndGradeId_Date(Long id, LocalDate date);

    boolean existsByGradeId_GradedCourse_IdAndGradeId_Date(Long id, LocalDate date);
}
