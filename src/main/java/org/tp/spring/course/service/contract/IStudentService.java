package org.tp.spring.course.service.contract;

import org.tp.spring.course.entity.Student;
import org.tp.spring.course.exceptions.StudentNotFoundException;

import java.util.List;

public interface IStudentService {
    Student getStudentById(Long id) throws StudentNotFoundException;

    List<Student> getStudentByNameAndFamilyName(String name, String familyName);

    Student createStudent(Student student);

    void deleteStudent(Long id) throws StudentNotFoundException;

    List<Student> getAllStudents();

    boolean exists(Long id);
}
