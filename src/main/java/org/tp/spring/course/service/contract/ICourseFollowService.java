package org.tp.spring.course.service.contract;

import org.tp.spring.course.entity.CourseFollow;
import org.tp.spring.course.entity.Grading;
import org.tp.spring.course.entity.Student;
import org.tp.spring.course.exceptions.CourseFollowNotFoundException;
import org.tp.spring.course.exceptions.CourseNotFoundException;
import org.tp.spring.course.exceptions.GradingNotFoundException;
import org.tp.spring.course.exceptions.StudentNotFoundException;

import java.time.LocalDate;
import java.util.List;


public interface ICourseFollowService {
    CourseFollow createCourseFollow(CourseFollow courseFollow) throws CourseNotFoundException, StudentNotFoundException;

    CourseFollow getCourseFollowById(Long id) throws CourseFollowNotFoundException;

    void deleteCourseFollow(Long id) throws CourseFollowNotFoundException;

    Grading addGrade(Grading grading) throws CourseFollowNotFoundException;

    List<Student> getStudentsByMark(long course, float mark);

    void deleteGradeByFollowIdAndDate(long id, LocalDate date) throws GradingNotFoundException;

    boolean exists(long id);

}
