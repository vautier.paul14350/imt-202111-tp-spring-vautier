package org.tp.spring.course.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDate;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class GradePK implements Serializable {
    private LocalDate date;
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "follow_id", referencedColumnName = "id")
    CourseFollow gradedCourse;
}
