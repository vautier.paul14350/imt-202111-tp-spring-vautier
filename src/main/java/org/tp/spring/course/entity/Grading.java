package org.tp.spring.course.entity;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.*;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "grade")
public class Grading {
    @JsonUnwrapped
    @EmbeddedId
    GradePK gradeId;

    private Double grade;

}
