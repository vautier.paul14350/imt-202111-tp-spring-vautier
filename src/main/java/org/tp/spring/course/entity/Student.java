package org.tp.spring.course.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.util.Collections;
import java.util.Set;

@Entity
@Table(name = "student")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@RequiredArgsConstructor
public class Student {
    @Id
    //@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic
    @Column
    @NonNull
    private String name;

    @Basic
    @Column(name = "family_name")
    @NonNull
    private String familyName;

    @OneToMany(mappedBy = "student", fetch = FetchType.LAZY)
    @JsonManagedReference
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ToString.Exclude
    private Set<CourseFollow> followedCourses = Collections.emptySet();

    public Student(Long id) {
        this.id = id;
    }
}
