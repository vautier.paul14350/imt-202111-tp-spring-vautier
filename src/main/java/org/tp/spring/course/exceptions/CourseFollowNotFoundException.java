package org.tp.spring.course.exceptions;


public class CourseFollowNotFoundException extends Exception{
    public CourseFollowNotFoundException(Long id) {
        super("Couldn't find a CourseFollow with id : "+ id);
    }
}
