package org.tp.spring.course.exceptions;

import java.time.LocalDate;

public class GradingNotFoundException extends Exception {
    public GradingNotFoundException(LocalDate date, Long id) {
        super("Couldn't find a Grading for CourseFollow : " + id + " at date : " + date);
    }
}
