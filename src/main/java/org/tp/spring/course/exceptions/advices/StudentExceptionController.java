package org.tp.spring.course.exceptions.advices;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.tp.spring.course.exceptions.StudentNotFoundException;

@ControllerAdvice
public class StudentExceptionController {

    @ExceptionHandler(value = StudentNotFoundException.class)
    public ResponseEntity<Object> notFound(StudentNotFoundException exception) throws JSONException {
        JSONObject response = new JSONObject();
        response.put("status", "error");
        response.put("message", exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response.toString());
    }
}
