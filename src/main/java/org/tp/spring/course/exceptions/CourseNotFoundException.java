package org.tp.spring.course.exceptions;

public class CourseNotFoundException extends Exception {
    public CourseNotFoundException(Long id) {
        super("Couldn't find a Course with id : " + id);
    }
}
