package org.tp.spring.course.exceptions;

public class StudentNotFoundException extends Exception {
    public StudentNotFoundException(Long id) {
        super("Couldn't find a Student with id : " + id);
    }
}
