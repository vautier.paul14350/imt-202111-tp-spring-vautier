package org.tp.spring.course.mapstruct.mappers;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.tp.spring.course.entity.Course;
import org.tp.spring.course.entity.CourseFollow;
import org.tp.spring.course.entity.Grading;
import org.tp.spring.course.entity.Student;
import org.tp.spring.course.mapstruct.dtos.*;

import java.util.List;

@Mapper(
        componentModel = "spring"
)
// implemented at compilation by MapStruct
public interface MapStructMapper {

    List<CourseGetDto> coursesListToCourseAllDto(List<Course> courses, @Context CycleAvoidingMappingContext context);

    CourseGetDto courseToCourseGetDto(Course course, @Context CycleAvoidingMappingContext context);

    CourseFollowGetDto courseFollowToCourseFollowDto(CourseFollow courseFollow, @Context CycleAvoidingMappingContext context);

    @Mapping(target = "student.id", source = "studentId")
    @Mapping(target = "course.id", source = "courseId")
    CourseFollow courseFollowPostDtoToCourseFollow(CourseFollowPostDto courseFollow);

    Course coursePostDtoToCourse(CoursePostDto coursePostDto, @Context CycleAvoidingMappingContext context);

    @Mapping(target = "date", source = "grading.gradeId.date")
    GradeDto gradeToGradingDto(Grading grading, @Context CycleAvoidingMappingContext context);

    @Mapping(target = "gradeId.gradedCourse.id", source = "followId")
    @Mapping(target = "gradeId.date", source = "date")
    Grading gradingPostDtoToGrading(GradingPostDto gradingPostDto);

    StudentGetDto studentToStudentGetDto(Student student, @Context CycleAvoidingMappingContext context);

    Student studentPostDtoToStudent(StudentPostDto studentPostDto, @Context CycleAvoidingMappingContext context);

    List<StudentGetDto> studentsListToStudentListGetDto(List<Student> studentList, @Context CycleAvoidingMappingContext context);
}