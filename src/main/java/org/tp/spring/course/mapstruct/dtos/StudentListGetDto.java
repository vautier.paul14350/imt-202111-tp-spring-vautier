package org.tp.spring.course.mapstruct.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StudentListGetDto {

    List<StudentGetDto> listStudentGetDto;
}
