package org.tp.spring.course.mapstruct.dtos;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GradeDto {
    @JsonProperty("grade")
    private Long grade;

    @JsonProperty
    private LocalDate date;

    @JsonProperty("gradedCourse")
    @JsonBackReference
    private CourseFollowGetDto gradedCourse;

}
