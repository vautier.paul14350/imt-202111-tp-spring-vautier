package org.tp.spring.course.mapstruct.dtos;

import com.fasterxml.jackson.annotation.JsonIncludeProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class CourseFollowGetDto {
    @JsonProperty(value = "id", access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @JsonProperty("student")
    @JsonIncludeProperties("id")
    @JsonUnwrapped(prefix = "student_")
    private StudentGetDto student;

    @JsonProperty("course")
    @JsonIncludeProperties("id")
    @JsonUnwrapped(prefix = "course_")
    private CourseGetDto course;

    @JsonProperty("grades")
    @JsonManagedReference
    private Set<GradeDto> gradings;
}
