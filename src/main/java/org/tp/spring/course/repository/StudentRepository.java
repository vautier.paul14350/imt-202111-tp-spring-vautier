package org.tp.spring.course.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.tp.spring.course.entity.Student;

import java.util.List;

@Repository
@Transactional
public interface StudentRepository extends CrudRepository<Student, Long> {
    @Query(value = "SELECT s FROM Student s " +
            "JOIN CourseFollow c on c.student.id = s.id " +
            "WHERE c.course.id = :courseId and c.id in(" +
            "SELECT g.gradeId.gradedCourse.id FROM Grading g" +
            "   GROUP BY g.gradeId.gradedCourse.id" +
            "   HAVING avg(g.grade) < :mark" +
            ")")
    List<Student> findStudentByMark(long courseId, double mark);

    List<Student> findByFamilyNameAndName(String familyName, String name);
}
