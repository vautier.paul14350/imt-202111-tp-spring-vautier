package org.tp.spring.course.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.tp.spring.course.entity.CourseFollow;

import java.util.Set;


@Repository
@Transactional
public interface CourseFollowRepository extends CrudRepository<CourseFollow, Long> {
    @Query(value = "SELECT c FROM CourseFollow c " +
            "WHERE c.course.id = :courseId and c.id in(" +
            "SELECT g.gradeId.gradedCourse.id FROM Grading g" +
            "   GROUP BY g.gradeId.gradedCourse.id" +
            "   HAVING avg(g.grade) < :mark" +
            ")")
    Set<CourseFollow> findCoursesBelowGrade(long courseId, double mark);
}
