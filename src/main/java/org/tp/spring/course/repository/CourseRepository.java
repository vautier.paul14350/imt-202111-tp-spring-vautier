package org.tp.spring.course.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.tp.spring.course.entity.Course;

@Repository
@Transactional
public interface CourseRepository extends CrudRepository<Course, Long> {
}
