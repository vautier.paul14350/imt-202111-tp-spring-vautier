package org.tp.spring.course.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.tp.spring.course.entity.Grading;
import org.tp.spring.course.exceptions.CourseFollowNotFoundException;
import org.tp.spring.course.exceptions.CourseNotFoundException;
import org.tp.spring.course.exceptions.GradingNotFoundException;
import org.tp.spring.course.exceptions.StudentNotFoundException;
import org.tp.spring.course.mapstruct.dtos.CourseFollowGetDto;
import org.tp.spring.course.mapstruct.dtos.CourseFollowPostDto;
import org.tp.spring.course.mapstruct.dtos.GradingPostDto;
import org.tp.spring.course.mapstruct.dtos.StudentGetDto;
import org.tp.spring.course.mapstruct.mappers.CycleAvoidingMappingContext;
import org.tp.spring.course.mapstruct.mappers.MapStructMapper;
import org.tp.spring.course.service.contract.ICourseFollowService;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/follow")
@Slf4j
@AllArgsConstructor
public class CourseFollowController {
    private final ICourseFollowService courseFollowService;
    private final MapStructMapper mapStructMapper;

    @GetMapping("/{id}")
    public CourseFollowGetDto getCourseFollow(@PathVariable Long id) throws CourseFollowNotFoundException {
        return mapStructMapper.courseFollowToCourseFollowDto(courseFollowService.getCourseFollowById(id), new CycleAvoidingMappingContext());
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public Long createCourseFollow(@RequestBody CourseFollowPostDto postDto) throws CourseNotFoundException, StudentNotFoundException {
        return courseFollowService.createCourseFollow(
                mapStructMapper.courseFollowPostDtoToCourseFollow(postDto)
        ).getId();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCourseFollow(@PathVariable Long id) throws CourseFollowNotFoundException {
        courseFollowService.deleteCourseFollow(id);
    }

    @PostMapping("/{id}/grade")
    @ResponseStatus(HttpStatus.CREATED)
    public Grading createGrade(@RequestBody GradingPostDto gradingPostDto, @PathVariable Long id) throws CourseFollowNotFoundException {
        gradingPostDto.setFollowId(id);
        return courseFollowService.addGrade(
                mapStructMapper.gradingPostDtoToGrading(gradingPostDto)
        );
    }

    @DeleteMapping("/{id}/grade/{date}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteGradeByFollowIdAndDate(@PathVariable Long id, @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) throws GradingNotFoundException {
        courseFollowService.deleteGradeByFollowIdAndDate(id, date);
    }

    @GetMapping("/{id}/below/{grade}")
    @ResponseStatus(HttpStatus.OK)
    public List<StudentGetDto> getStudentsByGradeLessThan(@PathVariable Long id, @PathVariable Long grade) {
        return mapStructMapper.studentsListToStudentListGetDto(
                courseFollowService.getStudentsByMark(id, grade), new CycleAvoidingMappingContext());
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Couldn't find a follow with the matching id")
    @ExceptionHandler({NoSuchElementException.class, EmptyResultDataAccessException.class})
    public void noStudentFoundExceptions() {
    }
}
