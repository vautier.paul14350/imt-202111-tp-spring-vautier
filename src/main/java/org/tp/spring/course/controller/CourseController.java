package org.tp.spring.course.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.tp.spring.course.exceptions.CourseNotFoundException;
import org.tp.spring.course.mapstruct.dtos.CourseGetDto;
import org.tp.spring.course.mapstruct.dtos.CoursePostDto;
import org.tp.spring.course.mapstruct.mappers.CycleAvoidingMappingContext;
import org.tp.spring.course.mapstruct.mappers.MapStructMapper;
import org.tp.spring.course.service.contract.ICourseService;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/courses")
@Slf4j
@AllArgsConstructor
public class CourseController {
    private final ICourseService courseService;
    private final MapStructMapper mapStructMapper;

    @GetMapping("/{id}")
    public CourseGetDto getCourse(@PathVariable Long id) throws CourseNotFoundException {
        return mapStructMapper.courseToCourseGetDto(courseService.getCourseById(id), new CycleAvoidingMappingContext());
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public Long createCourse(@RequestBody CoursePostDto coursePostDto) {
        return courseService.createCourse(
                mapStructMapper.coursePostDtoToCourse(coursePostDto, new CycleAvoidingMappingContext())
        ).getId();
    }

    @GetMapping("")
    public List<CourseGetDto> getAllCourses() {
        return mapStructMapper.coursesListToCourseAllDto(courseService.getAllCourses(), new CycleAvoidingMappingContext());
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteCourse(@PathVariable Long id) throws CourseNotFoundException {
        courseService.deleteCourse(id);
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Couldn't find a course with the matching id")
    @ExceptionHandler({NoSuchElementException.class, EmptyResultDataAccessException.class})
    void noCourseFoundExceptions() {
    }

}
