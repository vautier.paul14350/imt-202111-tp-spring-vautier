package org.tp.spring.course.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.tp.spring.course.exceptions.StudentNotFoundException;
import org.tp.spring.course.mapstruct.dtos.StudentGetDto;
import org.tp.spring.course.mapstruct.dtos.StudentPostDto;
import org.tp.spring.course.mapstruct.mappers.CycleAvoidingMappingContext;
import org.tp.spring.course.mapstruct.mappers.MapStructMapper;
import org.tp.spring.course.service.contract.IStudentService;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/students")
@Slf4j
@AllArgsConstructor
public class StudentController {
    private final MapStructMapper mapStructMapper;
    private final IStudentService studentService;

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public StudentGetDto getStudentById(@PathVariable(value = "id") Long id) throws StudentNotFoundException {
        return mapStructMapper.studentToStudentGetDto(
                studentService.getStudentById(id), new CycleAvoidingMappingContext());
    }

    // get a Student by his name & family name --> get Student & enrolled courses
    @GetMapping("/query")
    @ResponseStatus(HttpStatus.OK)
    public List<StudentGetDto> getStudentByNameAndFamilyName(@RequestParam(name = "name") String name, @RequestParam(name = "family_name") String familyName) {
        return mapStructMapper.studentsListToStudentListGetDto(studentService.getStudentByNameAndFamilyName(name, familyName), new CycleAvoidingMappingContext());
    }

    // get all Students
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public List<StudentGetDto> getAllStudents() {
        return mapStructMapper.studentsListToStudentListGetDto(studentService.getAllStudents(), new CycleAvoidingMappingContext());
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public Long createStudent(@RequestBody StudentPostDto studentPostDto) {
        log.info("Created element :{}", studentPostDto);
        return studentService.createStudent(
                mapStructMapper.studentPostDtoToStudent(studentPostDto, new CycleAvoidingMappingContext())
        ).getId();
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteStudent(@PathVariable Long id) throws StudentNotFoundException {
        studentService.deleteStudent(id);
    }

    /*
    TODO :
    * enroll a student to a course
    * given a student, add a grade/mark to a enrolled course
     */

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Couldn't find a student with the matching id")
    @ExceptionHandler({NoSuchElementException.class, EmptyResultDataAccessException.class})
    public void noStudentFoundExceptions() {
    }

}
